# Ohmyfood!

Ohmyfood! est une entreprise de commande de repas en ligne, permettant aux utilisateurs de composer leur propre menu et ainsi réduire leur temps d’attente dans les restaurants.

## Versions

[Dernière version](https://arnaudin.gitlab.io/arnaudin_3_25052021)

## Environnement

- npm
- sass

## Auteurs et remerciement

- Arnaud In
- Jérôme Collomp
